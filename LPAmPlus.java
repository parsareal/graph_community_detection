import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class LPAmPlus {

    private int[][] vertices;           // Array of labels and degrees for each vertex
//    private double[][] bs; // For Low V
    private int m, n = 0;
    private Vector<int[]> order;        // An array for shuffling
    private Vector<Integer> communities;// Array of communities
    private Vector<Integer>[] list;     // Adjacency List

    public static void main(String[] args) {
        boolean flag = false;
        File file = null;
        Scanner scanner = new Scanner(System.in);
        LPAmPlus LPA = null;
        while (!flag) {
            System.out.print("Enter the File Path: ");
            try {
                file = new File(scanner.nextLine());
                LPA = new LPAmPlus(file);
                flag = true;
            } catch (FileNotFoundException e) {
                System.out.println("File Not Found!");
            }
        }
        scanner.close();

        float NMI = 0;
        long initTime = 0;
        long lpamTime = 0;
        int k = 1;
        for (int i = 0; i < k; i++) {
            try {
                long time = System.currentTimeMillis();
                LPA = new LPAmPlus(file);
                initTime += System.currentTimeMillis() - time;
                time = System.currentTimeMillis();
                Vector<Integer> list = LPA.LPAmP();
                lpamTime += System.currentTimeMillis() - time;
                NMI += NMI(list, file.getParent() + "/community.txt");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.out.println("Initialization Duration: " + initTime/k);
        System.out.println("LPAm Duration: " + lpamTime/k);
        System.out.println("Average NMI: " + NMI/k);

//        while (true) {
//            try {
//                LPA = new LPAmPlus(file);
//                NMI = NMI(LPA.LPAmP(), "/home/ssgums/MEGA/IDEAProjects/LPAm+/resources/N1000MU.5/community.txt");
////                System.out.println(NMI);
//            } catch (FileNotFoundException e) {
//                System.out.println("File Not Found!");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
    }

    // Constructor
    private LPAmPlus(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file); // Specify N
//        scanner.useDelimiter("\\s*,\\s*|\\s*\\r\\n\\s*|\\s*\\n\\s*"); //For CSV
        while (scanner.hasNext()) {
            int temp = scanner.nextInt();
            if (temp > n)
                n = temp;
        }

        order = new Vector<>(); // Initialize Fields
        communities = new Vector<>();
        vertices = new int[2][n];
        list = new Vector[n];
//        bs = new double[n][n]; // For Low V
//        boolean[][] graph = new boolean[n][n];
        for (int i = 0; i < n; i++) {
            order.add(new int[] {i, i});
            vertices[1][i] = i;
            communities.add(i);
            list[i] = new Vector<>();
        }

        scanner.close();
        scanner = new Scanner(file); // Set Edges
//        scanner.useDelimiter("\\s*,\\s*|\\s*\\r\\n\\s*|\\s*\\n\\s*");
        while (scanner.hasNext()) {
            int x = scanner.nextInt() - 1;
            int y = scanner.nextInt() - 1;
            list[x].add(y);
//            graph[x][y] = true; // For Low V
            vertices[0][x]++;
//            graph[0][x][y] = 1; // For Matrix
//            graph[0][y][x] = 1;
            m++;
        }
        m /= 2;

//        for (int i = 0; i < n; i++) { // Set Bs // For Low V
//            for (int j = 0; j < n; j++) {
////                if (i == j)
////                    graph[i][j] = true;
//                if (j >= i) {
//                    bs[i][j] = vertices[0][i] * vertices[0][j];
//                    bs[i][j] /= 2*m;
//                    bs[i][j] = (graph[i][j] ? 1 : 0) - bs[i][j];
//                } else
//                    bs[i][j] = bs[j][i];
//            }
//        }

        scanner.close();
    }

    // LPAm+ Function
    private Vector<Integer> LPAmP() {
//        System.out.println("LPAm+ Started.");
//        System.out.println("LPAm Started.");
        LPAm();
//        System.out.println("LPAm Started.");
//        LPAm();
//        System.out.println("LPAm Started.");
//        LPAm();
//
//        releaseOutput();
//
//        boolean flag = true;
//        while (flag) {
//            System.out.println("Merge Started.");
//            flag = merge();
//            System.out.println("LPAm Started.");
//            LPAm();
//        }

//        releaseOutput();

        Integer[] labels = new Integer[n + 1];
        Vector<Integer> vector = new Vector<>();
        for (int i = 0; i < n; i++) {
            if (!vector.contains(vertices[1][i]))
                vector.add(vertices[1][i]);
            labels[i + 1] = vector.indexOf(vertices[1][i]) + 1;
        }
        return new Vector<Integer>(Arrays.asList(labels));
    }

    // LPAm Function
    private void LPAm() {
        boolean flag;
        int k = 0;
        do {
            k++;
//            System.out.println(k);
            flag = false;
//            if (k > 100)
//                releaseOutput();
            Collections.shuffle(order); // Generate a Permutation
            for (int i = 0; i < n; i++) { // Copy Old Ls
                order.get(i)[1] = vertices[1][order.get(i)[0]];
            }
            for (int i = 0; i < n; i++) { // Calculate New Ls
                boolean f = l(order.get(i)[0], i);
                if (!flag)
                    flag = f;
            }
        } while (flag);

//        communities.removeAllElements(); // Update Communities
//        for (int i = 0; i < n; i++)
//            if (!communities.contains(vertices[1][i]))
//                communities.add(vertices[1][i]);
    }

    // L Calculator
    private boolean l(int vertex, int k) {
        HashMap<Integer, Double> labels = new HashMap<>(); // Count Ls
        for (int i = 0; i < list[vertex].size(); i++) {
//            int key = order.get(i)[1];
            int com = vertices[1][list[vertex].get(i)];
//            if (labels.containsKey(key)) // For Low V
//                labels.put(key, labels.get(key) + bs[vertex][i]);
//            else
//                labels.put(key, bs[vertex][i]);
            if (labels.containsKey(com))
                labels.put(com, labels.get(com) + b(vertex, list[vertex].get(i)));
            else
                labels.put(com, b(vertex, list[vertex].get(i)));
        }


        double Bmax = Integer.MIN_VALUE; // Find Max
        Vector<Integer> Lmax = new Vector<>();
        Set<Map.Entry<Integer, Double>> set = labels.entrySet();
        for (Map.Entry<Integer, Double> entry : set) {
            double b = entry.getValue();
            if (b > Bmax) {
                Bmax = b;
                Lmax.removeAllElements();
                Lmax.add(entry.getKey());
            } else if (b == Bmax)
                Lmax.add(entry.getKey());
        }

        if (Lmax.contains(vertices[1][vertex]))
            return false;

        Collections.shuffle(Lmax); // Shuffle Maxes
        vertices[1][vertex] = Lmax.get(0);
        return true;
//        int lNew = Lmax.get(0);
//        if (vertices[1][vertex] != lNew) {
//            vertices[1][vertex] = lNew;
//            return true;
//        }
//        return false;
    }

    // Merge Function
    private boolean merge() {
        boolean WhileContinue = false;
        int currentSize = communities.size();
        int i = 0;

        while (i < currentSize) {
            int j = i + 1;

            while (j < currentSize) {
                int[] coms = new int[n]; // Merge the two Communities temporary
                for (int k = 0; k < n; k++) {
                    if (vertices[1][k] == communities.get(j))
                        coms[k] = communities.get(i);
                    else
                        coms[k] = vertices[1][k];
                }

                if (modularity(coms) - modularity(vertices[1]) > 0) { // Check Modularity had increased
                    boolean merge = true;

                    for (int c = 0; c < currentSize; c++) { // Merge each Community with the two
                        if (c != j && c != i) {
                            int[] coms1 = new int[n]; // Merge Community C with I & J temporary
                            int[] coms2 = new int[n];
                            for (int k = 0; k < n; k++) {
                                if (vertices[1][k] == communities.get(c)) {
                                    coms1[k] = communities.get(i);
                                    coms2[k] = communities.get(j);
                                } else {
                                    coms1[k] = vertices[1][k];
                                    coms2[k] = vertices[1][k];
                                }
                            }

                            if (modularity(coms2) > modularity(coms) || modularity(coms1) > modularity(coms)) { // Check the two are the best match
                                merge = false;
                                break;
                            }
//                            else System.out.println(modularity(vertices[1]) + " " + modularity(coms) + " " + modularity(coms1) + " " + modularity(coms2));
                        }
                    }

                    if (merge) {
                        WhileContinue = true; // Merge the two Communities
                        for (int l = 0; l < n; l++) {
                            if (vertices[1][l] == communities.get(i))
                                vertices[1][l] = communities.get(j);
                        }

                        communities.set(i, communities.get(j)); // Remove one of them
                        communities.remove(j);
                        currentSize -= 1;
                    }
                }

                j++;
            }

            i++;
        }
        
        return WhileContinue;
    }

    // Modularity Calculator
    private double modularity(int[] communities) {
//        double q = 0; // For Low V
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < n; j++)
//                if (communities[i] == communities[j])
//                    q += bs[i][j];
//        q /= 2*m;
//        return q;
        double q = 0;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (communities[i] == communities[j])
                    q += b(i, j);
        q /= 2*m;
        return q;
    }

    // Calculate B
    private double b(int i, int j) {
        double b = vertices[0][i] * vertices[0][j];
        b /= 2*m;
        b = (i == j || list[i].contains(j) ? 1 : 0) - b;
        return b;
    }

    // Output
    private void releaseOutput() {
        HashMap<Integer, Vector<Integer>> labels = new HashMap<>(); // Create Label -> Vertices HashMap
        for (int i = 0; i < n; i++) {
            if (labels.containsKey(vertices[1][i])) {
                labels.get(vertices[1][i]).add(i);
            } else {
                Vector<Integer> v = new Vector<>();
                v.add(i);
                labels.put(vertices[1][i], v);
            }
        }

        int label = 1; // Print Output
        Set<Map.Entry<Integer, Vector<Integer>>> set = labels.entrySet();
        for (Map.Entry<Integer, Vector<Integer>> entry : set) {
            System.out.print("Label: " + label + "      Vertices: ");
            for (Integer vertex : entry.getValue())
                System.out.print(vertex + 1 + "   ");
            System.out.println();
            label++;
        }
    }

    // NMI
    private static float NMI(Vector<Integer> Prediction, String TrueCommunityPathTXT)throws Exception {
        Vector<Integer> TrueLabel = new Vector<Integer>();
        int countGuess = 0, countGold = 0;
        float NMI = 0, up = 0, down = 0;
        int n = 0;
        TrueLabel.add(0);
        BufferedReader br = new BufferedReader(new FileReader(TrueCommunityPathTXT));
        String line = br.readLine();
        while (line != null) {
            String[] parts = line.split(" ");
            TrueLabel.add(Integer.parseInt(parts[1]));
            n++;
            line = br.readLine();
        }
        br.close();
        if (n != Prediction.size() - 1)
            return -1;
        Hashtable<Integer, Integer> temp = new Hashtable<Integer, Integer>();
        int k = 1;
        for (int i = 1; i <= n; i++) {
            if (temp.containsKey((Integer) Prediction.get(i)))
                Prediction.set(i, temp.get(Prediction.get(i)));
            else {
                temp.put(Prediction.get(i), k);
                Prediction.set(i, temp.get(Prediction.get(i)));
                k++;
            }
        }
        for (int i = 1; i <= n; i++) {
            if (Prediction.get(i) > countGuess)
                countGuess = Prediction.get(i);
            if (TrueLabel.get(i) > countGold)
                countGold = TrueLabel.get(i);
        }
        float NRow[] = new float[countGold];
        float NCol[] = new float[countGuess];
        float matrix[][] = new float[countGold][countGuess];
        for (int i = 0; i < countGold; i++)
            matrix[i] = new float[countGuess];
        for (int i = 1; i <= n; i++) {
            matrix[TrueLabel.get(i) - 1][Prediction.get(i) - 1]++;
            NRow[TrueLabel.get(i) - 1]++;
            NCol[Prediction.get(i) - 1]++;
        }
        for (int i = 0; i < countGold; i++)
            if (NRow[i] != 0)
                down += NRow[i] * Math.log(NRow[i] / (float) (n)) / Math.log(2);
        for (int i = 0; i < countGuess; i++)
            if (NCol[i] != 0)
                down += NCol[i] * Math.log(NCol[i] / (float) (n)) / Math.log(2);
        for (int i = 0; i < countGold; i++)
            for (int j = 0; j < countGuess; j++)
                if (matrix[i][j] != 0)
                    up += matrix[i][j] * Math.log((matrix[i][j] * (float) (n) / ((NRow[i] * NCol[j])))) / Math.log(2);
        up *= (float) -2;
        NMI = up / down;
        return NMI;
    }

}
